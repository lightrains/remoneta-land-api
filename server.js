"use strict";

const Hapi = require("hapi");
const glob = require("glob");
const path = require("path");

// const Inert = require("inert");
// const Vision = require("vision");
const Pack = require("./package");

let fs = require("fs");
let writeStream = fs.createWriteStream("./output.log", { flags: "a" });

require("dotenv").config();

const server = Hapi.Server({
  port: process.env.APP_PORT,
  host: process.env.APP_SERVER
});

const init = async () => {
  // await server.register({
  //   plugin: require("./plugins/util.js")
  // });
  await server.register({
    plugin: require("hapi-pino"),
    options: {
      prettyPrint: true,
      logPayload: true,
      stream: writeStream
    }
  });

  glob
    .sync("./routes/*.js", {
      root: __dirname
    })
    .forEach(file => {
      const route = require(path.join(__dirname, file));
      server.route(route);
    });
  server.app.domain = server.info.uri;
  server.app.apiversion = Pack.version;

  await server.start();
  return server;
};

process.on("unhandledRejection", error => {
  console.log(error);
  process.exit();
});

init()
  .then(server => {
    console.log("Server running at:", server.info.uri);
  })
  .catch(error => {
    console.log(error);
  });
