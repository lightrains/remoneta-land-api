("use strict");

require("dotenv").config();

let HDWalletProvider = require("truffle-hdwallet-provider");
const Web3 = require("web3");
const contract = require("truffle-contract");
const Land = require(process.cwd() + "/build/contracts/Land.json");
const Remoneta = require(process.cwd() + "/build/contracts/Remoneta.json");

function getWeb3Provider() {
  return new HDWalletProvider(
    process.env.MNENOMIC,
    "https://ropsten.infura.io/v3/" + process.env.INFURA_API_KEY
  );
}

if (typeof web3 !== "undefined") {
  var web3 = new Web3(web3.currentProvider);
} else {
  let provider = getWeb3Provider();
  var web3 = new Web3(provider);
}

function toWei(ethAmount) {
  return web3.utils.toWei(ethAmount, "ether");
}

function fromWei(ethAmount) {
  return web3.utils.fromWei(ethAmount, "ether");
}

function sendTokens(contractName, toAddress, amount) {
  //Promise.reject(new Error("fail"));

  const loadTokenAddress =
    contractName === "Land"
      ? process.env.ROPSTEN_LAND
      : process.env.ROPSTEN_REMONETA;

  const CDP = contract(contractName);
  CDP.setProvider(web3.currentProvider);
  var cdp = CDP.at(loadTokenAddress);

  return cdp.mint(toAddress, toWei(amount), {
    from: process.env.FROM_ADDRESS,
    gas: 210000
  });
}

function balanceOf(contractName, address) {
  const loadTokenAddress =
    contractName === "Land"
      ? process.env.ROPSTEN_LAND
      : process.env.ROPSTEN_REMONETA;

  const CDP = contract(contractName);
  CDP.setProvider(web3.currentProvider);
  var cdp = CDP.at(process.env.ROPSTEN_TOKEN);
  return cdp.balanceOf(loadTokenAddress);
}

exports.sendTokens = sendTokens;
exports.balanceOf = balanceOf;
exports.fromWei = fromWei;
