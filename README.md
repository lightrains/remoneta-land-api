# Remoneta-api document

- api document for sending tokens and checking balance for an account.

## Installation

### Prerequisites

- node version v9.11.2
- nvm - Node Version Manager(optional)

### Install

- Run `loadnvm`
- Switch to node stable `nvm use 9`
- Install dependencies `npm install`

## Start application

- Create configuration files `cp .env.example .env`
- Set up values in `.env` file `vim .env`
- Start Application `npm start`
- Server running at 127.0.0.1:3000

### API

#### base_url : http://52.214.214.217:3000

#### - {base_url}/testnet/sendToken

- method: POST
- parameters: contractName (Land or Remoneta),toAddress,amount(number)
- send given amount of token to the given address

#### - {base_url}/testnet/balance/{contractName}/{address}

- method: GET
- parameters:{contractName} (Land or Remoneta), {address}
- returns balance of given wallet address
