"use strict";

const Joi = require("joi");
const Relish = require("relish")();
const { sendTokens, balanceOf, fromWei } = require("../util");
const {
  sendTokensTestnet,
  balanceOfTestnet,
  isAddress
} = require("../utilTestnet");

module.exports = [
  {
    method: "POST",
    path: "/sendToken",
    config: {
      auth: false,
      description: "Sending tokens",
      notes: "Sending tokens",
      tags: ["api", "v1", "user"],
      handler: async (request, h) => {
        const contractName = request.payload.contractName;
        const toAddress = request.payload.toAddress;
        const amount = request.payload.amount;
        var response = h.response({
          message: "sending token"
        });
        if (!isAddress(toAddress)) {
          response = h.response({
            message: "Invalid address"
          });
          return response.code(400);
        }
        try {
          let token = sendTokens(contractName, toAddress, amount.toString());
          let result = await token.then(result => result);

          response = h.response({
            message: "Success",
            address: toAddress,
            tokens: amount,
            txid: result,
            createdAt: new Date()
          });
          response.code(200);
        } catch (e) {
          console.log(e);
          response.code(400);
        } finally {
          h.response(response);
          response.type("json");
          return response;
        }
        return response;
      },
      validate: {
        payload: {
          contractName: Joi.any()
            .valid("Land", "Remoneta")
            .required()
            .description("Invalid contract"),
          toAddress: Joi.string()
            .length(42)
            .regex(/^[0x]{2}/)
            .required()
            .description("wallet address"),
          amount: Joi.number()
            .required()
            .description("number of token")
        },
        failAction: Relish.failAction
      }
    }
  },
  {
    method: "GET",
    path: "/balance/{address}",
    config: {
      auth: false,
      description: "Balance of given address",
      notes: "Balance of given address",
      tags: ["api", "v1", "user"],
      handler: async (request, h) => {
        let response = h.response({
          message: "Balance"
        });
        const address = request.params.address;
        if (!isAddress(address)) {
          response = h.response({
            message: "Invalid address"
          });
          return response.code(400);
        }
        try {
          const balance = balanceOf(address);
          let result = await balance.then(result => {
            return fromWei(result.toString());
          });
          response = h.response({
            message: "AUG Token balance of " + address + " is " + result,
            data: result
          });
          response.code(200);
        } catch (e) {
          console.log(e);
          response.code(404);
        } finally {
          response = h.response(response);
          response.type("json");
          return response;
        }
        return response;
      },
      validate: {
        params: {
          address: Joi.string()
            .length(42)
            .regex(/^[0x]{2}/)
            .required()
            .description("wallet address")
        },
        failAction: Relish.failAction
      }
    }
  },
  {
    method: "POST",
    path: "/testnet/sendToken",
    config: {
      auth: false,
      description: "Sending tokens",
      notes: "Sending tokens",
      tags: ["api", "v1", "user"],
      handler: async (request, h) => {
        const contractName = request.payload.contractName;
        const toAddress = request.payload.toAddress;
        const amount = request.payload.amount;
        var response = h.response({
          message: "sending token"
        });
        if (!isAddress(toAddress)) {
          response = h.response({
            message: "Invalid address"
          });
          return response.code(400);
        }
        try {
          let token = sendTokensTestnet(
            contractName,
            toAddress,
            amount.toString()
          );
          let result = await token.then(result => result);

          response = h.response({
            message: "Success",
            address: toAddress,
            tokens: amount,
            txid: result,
            createdAt: new Date()
          });
          response.code(200);
        } catch (e) {
          console.log(e);
          response.code(400);
        } finally {
          h.response(response);
          response.type("json");
          return response;
        }
        return response;
      },
      validate: {
        payload: {
          contractName: Joi.any()
            .valid("Land", "Remoneta")
            .required()
            .description("Invalid contract"),
          toAddress: Joi.string()
            .length(42)
            .regex(/^[0x]{2}/)
            .required()
            .description("wallet address"),
          amount: Joi.number()
            .required()
            .description("number of token")
        },
        failAction: Relish.failAction
      }
    }
  },
  {
    method: "GET",
    path: "/testnet/balance/{contractName}/{address}",
    config: {
      auth: false,
      description: "Balance of given address",
      notes: "Balance of given address",
      tags: ["api", "v1", "user"],
      handler: async (request, h) => {
        let response = h.response({
          message: "Balance"
        });
        const contractName = request.params.contractName;
        const address = request.params.address;
        if (!isAddress(address)) {
          response = h.response({
            message: "Invalid address"
          });
          return response.code(400);
        }
        try {
          const balance = balanceOfTestnet(contractName, address);
          let result = await balance.then(result => {
            return fromWei(result.toString());
          });
          response = h.response({
            message:
              contractName + " Token balance of " + address + " is " + result,
            data: result
          });
          response.code(200);
        } catch (e) {
          console.log(e);
          response.code(404);
        } finally {
          response = h.response(response);
          response.type("json");
          return response;
        }
        return response;
      },
      validate: {
        params: {
          contractName: Joi.any()
            .valid("Land", "Remoneta")
            .required()
            .description("Invalid contract"),
          address: Joi.string()
            .length(42)
            .regex(/^[0x]{2}/)
            .required()
            .description("wallet address")
        },
        failAction: Relish.failAction
      }
    }
  }
];
