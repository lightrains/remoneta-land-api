("use strict");

require("dotenv").config();

let HDWalletProvider = require("truffle-hdwallet-provider");
const Web3 = require("web3");
const contract = require("truffle-contract");
const Land = require(process.cwd() + "/build/contracts/Land.json");
const Remoneta = require(process.cwd() + "/build/contracts/Remoneta.json");

function getWeb3Provider() {
  return new HDWalletProvider(
    process.env.MNENOMIC,
    "https://ropsten.infura.io/v3/" + process.env.INFURA_API_KEY
  );
}

if (typeof web3 !== "undefined") {
  var web3 = new Web3(web3.currentProvider);
} else {
  let provider = getWeb3Provider();
  var web3 = new Web3(provider);
}

function toWei(ethAmount) {
  return web3.utils.toWei(ethAmount, "ether");
}

function fromWei(ethAmount) {
  return web3.utils.fromWei(ethAmount, "ether");
}

function isAddress(address) {
  return web3.utils.isAddress(address);
}

function sendTokensTestnet(contractName, toAddress, amount) {
  //Promise.reject(new Error("fail"));

  const loadTokenAddress =
    contractName === "Land"
      ? process.env.ROPSTEN_LAND
      : process.env.ROPSTEN_REMONETA;

  const activeContract = contractName === "Land" ? Land : Remoneta;

  const CDP = contract(activeContract);
  CDP.setProvider(web3.currentProvider);
  var cdp = CDP.at(loadTokenAddress);

  console.log(process.env.ROPSTEN_FROM_ADDRESS);

  return cdp.mint(toAddress, toWei(amount), {
    from: process.env.ROPSTEN_FROM_ADDRESS,
    gas: 210000
  });
}

function balanceOfTestnet(contractName, address) {
  const loadTokenAddress =
    contractName === "Land"
      ? process.env.ROPSTEN_LAND
      : process.env.ROPSTEN_REMONETA;
  const activeContract = contractName === "Land" ? Land : Remoneta;

  const CDP = contract(activeContract);
  CDP.setProvider(web3.currentProvider);
  var cdp = CDP.at(loadTokenAddress);
  return cdp.balanceOf(address);
}

exports.sendTokensTestnet = sendTokensTestnet;
exports.balanceOfTestnet = balanceOfTestnet;
exports.fromWei = fromWei;
exports.isAddress = isAddress;
